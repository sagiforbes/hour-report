module hour-report.sagi.io

go 1.15

require (
	github.com/360EntSecGroup-Skylar/excelize/v2 v2.3.1
	github.com/xuri/efp v0.0.0-20201016154823-031c29024257 // indirect
	golang.org/x/crypto v0.0.0-20201117144127-c1f2f97bffc9 // indirect
	golang.org/x/net v0.0.0-20201110031124-69a78807bb2b // indirect
	golang.org/x/text v0.3.4 // indirect
)
