package main

import (
	"bufio"
	"bytes"
	"encoding/base64"
	"fmt"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"github.com/360EntSecGroup-Skylar/excelize/v2"
)

const excelFileTemplate = "hour-report-template.xlsx"

func readInput(textMessage string, defaultVal string) string {
	fmt.Print(textMessage)
	var inval string
	reader := bufio.NewReader(os.Stdin)
	inval, _ = reader.ReadString('\n')
	inval = strings.TrimSpace(inval)
	if inval == "" {
		inval = defaultVal
	}
	return inval
}

func generateHourReport(name string, year int, month int, startDayOfMonth, endDayOfMonth int) (string, error) {
	var err error

	templateContent, err := base64.StdEncoding.DecodeString(xmlTemplateCode64)
	templateReader := bytes.NewReader(templateContent)
	f, err := excelize.OpenReader(templateReader)
	if err != nil {
		return "", err
	}

	fileName := fmt.Sprintf("hour_report_%s_%d_%s.xlsx", name, year, time.Month(month).String())
	os.Remove(fileName)
	if err != nil {
		return "", err
	}

	f.SetActiveSheet(0)
	hourReportSheet := f.GetSheetName(0)
	var hoursStartColumn = 1
	var hoursStartRow = 22

	var startTime time.Time
	var endTime = time.Date(year, time.Month(month), endDayOfMonth, 0, 0, 0, 0, time.UTC)
	if startDayOfMonth >= endDayOfMonth {
		prevYear, prevMonth, _ := time.Date(year, time.Month(month), 1, 0, 0, 0, 0, time.UTC).Add(-24 * time.Hour).Date()
		startTime = time.Date(prevYear, time.Month(prevMonth), startDayOfMonth, 0, 0, 0, 0, time.UTC)
	} else {
		startTime = time.Date(year, time.Month(month), startDayOfMonth, 0, 0, 0, 0, time.UTC)
	}

	var row = 0
	var axis string
	var val string

	var start, end time.Time
	var dayDuration, TotalDuration time.Duration

	f.SetCellStr(hourReportSheet, "B3", name)
	f.SetCellStr(hourReportSheet, "B4", endTime.Month().String()[0:3])
	f.SetCellInt(hourReportSheet, "B5", endTime.Year())

	for curTime := startTime; curTime.Before(endTime) || curTime == endTime; curTime = curTime.Add(24 * time.Hour) {
		axis, err = excelize.CoordinatesToCellName(hoursStartColumn, row+hoursStartRow)

		if err != nil {
			fmt.Println(err)
			row++
			continue
		}
		val = strconv.Itoa(curTime.Day())
		fmt.Printf("Cell %s Day of month: %s\n", axis, val)
		err = f.SetCellStr(hourReportSheet, axis, val)
		if err != nil {
			return "", err
		}

		if curTime.Weekday() != 5 && curTime.Weekday() != 6 {
			start = time.Date(curTime.Year(), curTime.Month(), curTime.Day(), 9, 0, 0, 0, time.UTC)
			axis, err = excelize.CoordinatesToCellName(hoursStartColumn+1, row+hoursStartRow)
			f.SetCellValue(hourReportSheet, axis, start)

			end = time.Date(curTime.Year(), curTime.Month(), curTime.Day(), 18, 0, 0, 0, time.UTC)
			axis, err = excelize.CoordinatesToCellName(hoursStartColumn+2, row+hoursStartRow)
			f.SetCellValue(hourReportSheet, axis, end)

			dayDuration = end.Sub(start)
			TotalDuration += dayDuration
			axis, err = excelize.CoordinatesToCellName(hoursStartColumn+3, row+hoursStartRow)
			f.SetCellValue(hourReportSheet, axis, dayDuration)
		}
		row++
	} //for

	f.SetCellValue(hourReportSheet, "D53", TotalDuration)
	err = f.SaveAs(fileName)
	if err != nil {
		return "", err
	}

	filePath, err := filepath.Abs(fileName)
	if err != nil {
		return "", nil
	}
	return filePath, nil
}

func main() {
	var today = time.Now()
	dyear, dmonth, _ := today.Date()
	dstartDate := 25
	dendDate := 24

	month, err := strconv.Atoi(readInput(fmt.Sprintf("Report month (1-12) [%d]:", int(dmonth)), strconv.Itoa(int(dmonth))))
	if err != nil {
		fmt.Println("Failed to read month of the year: ", err)
		os.Exit(1)
	}
	startDayOfMonth, err := strconv.Atoi(readInput(fmt.Sprintf("Starting date [%d]:", dstartDate), strconv.Itoa(dstartDate)))
	if err != nil {
		fmt.Println("Failed to read start day of the month: ", err)
		os.Exit(1)
	}
	endDayOfMonth, err := strconv.Atoi(readInput(fmt.Sprintf("Starting date [%d]:", dendDate), strconv.Itoa(dendDate)))
	if err != nil {
		fmt.Println("Failed to read end day of the month: ", err)
		os.Exit(1)
	}
	year, err := strconv.Atoi(readInput(fmt.Sprintf("Report year [%d]", dyear), strconv.Itoa(dyear)))
	name := readInput(fmt.Sprintf("Employee Name [%s]: ", "Sagi Forbes Nagar"), "Sagi Forbes Nagar")
	fmt.Println()
	if pathName, err := generateHourReport(name, year, month, startDayOfMonth, endDayOfMonth); err == nil {
		fmt.Println("Hour report saved to: " + pathName)
	} else {
		fmt.Println("Failed to generate hour report: ", err)
	}

}
